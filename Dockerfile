################################################################################
#                                                                              #
#                                 {o,o}                                        #
#                                 |)__)                                        #
#                                 -"-"-                                        #
#                                                                              #
################################################################################
#
# Base data ontainer for work with symfony. It creates a data volume for
# Symfonys caching and other needs.
# Make sure that symfony is configured acordingly
#
#################################---FROM---#####################################

FROM alpine

################################################################################

#################################---ENV---######################################

ENV BASE_CACHE_DIRECTORY="/var/symfony"

################################################################################

#################################---INFO---#####################################

MAINTAINER Lovrenc Avsenek <a.lovrenc@gmail.com>

################################################################################

###############################---VOLUMES---####################################

VOLUME ${BASE_CACHE_DIRECTORY}

################################################################################

################################---BUILD---#####################################

RUN mkdir -pm 777 "${BASE_CACHE_DIRECTORY}/auth/cache" \
  "${BASE_CACHE_DIRECTORY}/auth/logs" \
  "${BASE_CACHE_DIRECTORY}/api/cache" \
  "${BASE_CACHE_DIRECTORY}/api/logs"

################################################################################

#################################---CMD---######################################

CMD

################################################################################
